# Jekyll Theory

[![build status](https://gitlab.com/rsakhale/Jekyll-Theory/badges/master/build.svg)](https://gitlab.com/rsakhale/Jekyll-Theory/commits/master) [![Gem Version](https://badge.fury.io/rb/jekyll-theory.svg)](https://badge.fury.io/rb/jekyll-theory) [![Download Theme](https://img.shields.io/badge/jekyll--theme-download-green.svg)](https://gitlab.com/rsakhale/Jekyll-Theory/repository/archive.zip?ref=master) [![Demo Theme](https://img.shields.io/badge/jekyll--theme-demo-blue.svg)](https://rsakhale.gitlab.io/Jekyll-Theory/demo/)

Just a crisp, modern landing page template ideal for businesses and corporations, designed by [Templated.co](https://templated.co). This template is perfect to display your services, team, blog.

## Installation

Add this line to your Jekyll site's Gemfile:

```ruby
gem "jekyll-theory", "~> 0.1.9"
```

And add this line to your Jekyll site:

```yaml
gems:
	- jekyll-bower
  - liquid-google-analytics
bower:
  theory: "https://templated.co/theory/download"
theme: jekyll-theory
```

And then execute:

```
$ bundle install
```

## Usage

This template comes with ready features available like Social Links, Team Info and Services on top of Posts which requires to be configured. Below are several configuration available that can be leveraged easily

### _config.yml

```yaml
social_links:
  facebook_url: https://facebook.com/SaiAshirwadInformatia
  twitter_url: https://twitter.com/SaiAshirwadInfo/
  linkedin_url: LINKEDIN_URL
  pinterest_url: PINTEREST_URL
  vimeo_url: VIMEO_URL
  email_id: EMAIL_ID


collections:
  services:
    output: true
  team:
    output: true
```

Now add two new folders named `_team` and `_services` that contain your team member info and services info individually as a markdown file

#### Front Matter for services

```yaml
title: "Interdum lorem pulvinar adipiscing vitae"
display: "Interdum lorem pulvinar<br> adipiscing vitae"
short_description: Morbi interdum mollis sapien. Sed ac risus. Phasellus lacinia, magna a ullamcorper laoreet, lectus arcu.
layout: service
```

#### Front Matter for team

```yaml
name: Dolore
username: dolore
title: Praesent placer
image: bower_components/theory/images/pic06.jpg
front_display: true
```

### Available Layouts

1. default.html - Presents the default layout that can be extended by any layout for base structure.
1. page.html - Presents the additional custom pages you can add
1. post.html - Presents the post page and also displays next and previous blog post
1. services.html - Presents the service information and also lists down additional services available

## Contributing

Issue reports, feature/enhancement requests and merge requests are welcome on GitLab.

## Development

To set up your environment to develop this theme, run `bundle install`.

You theme is setup just like a normal Jelyll site! To test your theme, run `bundle exec jekyll serve` and open your browser at `http://localhost:4000`. This starts a Jekyll server using your theme. Add pages, documents, data, etc. like normal to test your theme's contents. As you make modifications to your theme and to your content, your site will regenerate and you should see the changes in the browser after a refresh, just like normal.

When your theme is released, only the files in `_layouts`, `_includes`, and `_sass` tracked with Git will be released.

## License

The theme is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).

