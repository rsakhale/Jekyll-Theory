---
title: About
layout: demo_preview
permalink: /about/
---
# About Jekyll-Theory

This theme is prepared from [theory templated by templated.co](https://templated.co/theory) and is released under [MIT License](http://opensource.org/licenses/MIT) for free.

Jekyll-Theory is a responsive ready to use template for Jekyll based static sites making it easy for developers to consume and start using.

This theme brings out of box support for listing services and team members, which makes it quite good enough for making professional services based website.