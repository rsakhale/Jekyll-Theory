# coding: utf-8

Gem::Specification.new do |spec|
  spec.name          = "jekyll-theory"
  spec.version       = "0.1.9"
  spec.authors       = ["Rohan Sakhale"]
  spec.email         = ["rs@saiashirwad.com"]

  spec.summary       = "Just a crisp, modern landing page template ideal for businesses and corporations."
  spec.homepage      = "https://rsakhale.gitlab.io/Jekyll-Theory"
  spec.license       = "MIT"

  spec.files = `git ls-files -z`.split("\x0").select do |f|
    f.match(%r{^(_(includes|layouts|sass|team|services|data)|(contact|elements|LICENSE|README)((\.(txt|md|markdown|html)|$)))}i)
  end
#  spec.files += Dir.glob('bower_components/**/*')

  spec.add_runtime_dependency "liquid-google-analytics", "~> 1.1.0"
  spec.add_runtime_dependency "jekyll", "~> 3.3.0"
  spec.add_runtime_dependency "jekyll-bower", "~> 1.1.1"
  spec.add_development_dependency "bundler", "~> 1.12"
  spec.add_development_dependency "rake", "~> 10.0"

  spec.post_install_message = "Thanks for Installing jekyll-theory by Sai Ashirwad Informatia & Design by Templated.co!!"
end